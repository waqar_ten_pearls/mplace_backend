'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let invoicesSchema = new schema({
    reviewer: {
        type: String,
        required: true
    },       
    project: {
        type: String,
        required: true
    },    
    position: {
        type: String,
        required: true
    },    
    role: {
        type: String,
        required: true
    },
    weekStartDate: {
        type: String,
        required: true
    },
    weekEndDate: {
        type: String,
        required: true
    },
    weekId: {
        type: Number,
        default: 0
    },
    totalWeekHours: {
        type: Number,
        default: 0
    },
    timezone: {
        type: String,
        default: ''
    },
    role: {
        type: String,
        required: true
    },
}, {
    timestamps: {}
});

invoicesSchema.index({
    email: 1
}, {
    unique: true
});

module.exports = mongoose.model('invoices', invoicesSchema);