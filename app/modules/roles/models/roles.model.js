'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let rolesSchema = new schema({
    roleType: {
        type: String,
        required: true
    },
    project: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: {}
});

module.exports = mongoose.model('roles', rolesSchema);