'use strict';

const mongoose = require('mongoose'),
    mongoose_timestamps = require('mongoose-timestamp'),
    schema = mongoose.Schema;

let loggerSchema = new schema({
    logType: { type: String }, // 1: Error, 2: Info
    params: {},
    error: {},
    errorMessage: {type: String, default: ''},
    accountType: { type: Number, default: 0 }, //1: Contractor, 2: Trucking Company, 3: Dumpsite, 4: Driver, 0: Admin
    accountId: { type: String, default: '' },
    endPoint: { type: String, default: '' }
});

loggerSchema.plugin(mongoose_timestamps);
loggerSchema.index({ priority: 1 }, { background: true, name: 'IDX_LOG_PRIORITY' });
loggerSchema.index({ logType: 1 }, { background: true, name: 'IDX_LOG_TYPE' });
loggerSchema.index({ userId: 1 }, { background: true, name: 'IDX_USER_ID' });

module.exports = mongoose.model('logs', loggerSchema);