const mongoose = require('mongoose'),
  logsModel = mongoose.model('logs'),
  winston = require('winston');

let logEvent = async (endPoint, params, accountId, accountType, logType, error) => {
    winston.error(error)

   	await new logsModel({
      logType,
		  params,
		  error,
		  errorMessage: error,
		  accountType,
		  accountId,
		  endPoint
   	}).save();

   	return;
};

module.exports = {
  logEvent
};