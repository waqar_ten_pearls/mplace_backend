'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let projectsSchema = new schema({
    codeName: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: {}
});

module.exports = mongoose.model('projects', projectsSchema);