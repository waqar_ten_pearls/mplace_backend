const commonLib = require('../../globals/global.library');

const validateFirmId = (req, res, next) => {
    req.assert('firmId', 1000).notEmpty();
    req.assert('firmId', 1001).isValidObjectId();

    commonLib.validationResponse('Error: ', req, next);
};

const validateProjectIds = (req, res, next) => {
    req.query.projectIds = req.query.projectIds ? req.query.projectIds.split(',') : [];

    req.assert('projectIds', 1002).notEmptyArray();
    req.assert('projectIds', 1010).isValidArrayOfObjectIds();

    commonLib.validationResponse('Error: ', req, next);
};

const validateRoleIds = (req, res, next) => {
    req.query.roleIds = req.query.roleIds ? req.query.roleIds.split(',') : [];

    req.assert('roleIds', 1004).notEmptyArray();
    req.assert('roleIds', 1011).isValidArrayOfObjectIds();

    commonLib.validationResponse('Error: ', req, next);
};

const validateInvoiceParams = (req, res, next) => {
    req.query.weekId = parseInt(req.query.weekId);
    req.query.weekYear = parseInt(req.query.weekYear);

    req.query.projectIds = req.query.projectIds ? req.query.projectIds.split(',') : [];
    req.query.roleIds = req.query.roleIds ? req.query.roleIds.split(',') : [];
    req.query.reviewerIds = req.query.reviewerIds ? req.query.reviewerIds.split(',') : [];

    req.assert('weekId', 1006).notEmpty();
    req.assert('weekId', 1007).isNumber();
    req.assert('weekId', 1007).isValidWeekId();

    req.assert('weekYear', 1008).notEmpty();
    req.assert('weekYear', 1009).isNumber();
    req.assert('weekYear', 1009).isValidYear();

    req.assert('projectIds', 1010).isValidArrayOfObjectIds();
    req.assert('roleIds', 1011).isValidArrayOfObjectIds();
    req.assert('reviewerIds', 1012).isValidArrayOfObjectIds();

    req.assert('timezone', 1013).notEmpty();
    req.assert('timezone', 1014).isValidTimezone();

    commonLib.validationResponse('Error: ', req, next);
};

module.exports = {
    validateFirmId,
    validateProjectIds,
    validateRoleIds,
    validateInvoiceParams
};