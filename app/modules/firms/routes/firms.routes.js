const middleWares = require('../middlewares/firms.middlewares');
const controllers = require('../controllers/firms.controllers');

module.exports = (app, version) => {
    app.get(
		version + 'firms',
    	controllers.fetchFirmsList
    );

    app.get(
        version + 'firm/:firmId/projects',
        middleWares.validateFirmId,
    	controllers.fetchFirmProjects
    );

    app.get(
        version + 'firm/project/roles',
        middleWares.validateProjectIds,
    	controllers.fetchFirmProjectRoles
    );

    app.get(
        version + 'firm/project/role-positions',
        middleWares.validateRoleIds,
    	controllers.fetchProjectRolePositions
    );

    app.get(
        version + 'firm/:firmId/invoices',
        middleWares.validateFirmId,
        middleWares.validateInvoiceParams,
    	controllers.fetchFirmInvoices
    );
};