'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let firmsSchema = new schema({
    name: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: {}
});

firmsSchema.index({
    name: 1
}, {
    unique: true
});

module.exports = mongoose.model('firms', firmsSchema);