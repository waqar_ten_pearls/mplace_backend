const logController = require('../../logger/controller/logging.controller');
const responseModule = require('../../../../config/response');
const moment = require('moment');
const momentTimezone = require('moment-timezone');

const mongoose = require('mongoose');
const firmsModel = mongoose.model('firms');
const projectsModel = mongoose.model('projects');
const rolesModel = mongoose.model('roles');
const positionsModel = mongoose.model('positions');
const invoicesModel = mongoose.model('invoices');

const fetchFirmsList = async (req, res, next) => {
    try{
        const limit = parseInt(req.query.limit) || 100;
        const offset = parseInt(req.query.offset) || 0;

        const firms = await firmsModel.find({
            isDeleted: false
        }, {
            name: 1
        }).collation({
            locale: "en" 
        }).sort({
            name: 1
        }).limit(limit)
        .skip(offset);

        return responseModule.successResponse(req, res, {
            success: 1,
            message: '',
            data: {
                firms
            }
        });
    } catch (err) {
        logController.logEvent(req.url, req.body, req.user ? req.user._id : null, req.user ? req.user.accountType : 0, 1, err);
        return next({
            msgCode: 1
        });
    }
};

const fetchFirmProjects = async (req, res, next) => {
    try{
        const firmId = req.params.firmId;
        const limit = parseInt(req.query.limit) || 100;
        const offset = parseInt(req.query.offset) || 0;

        const projects = await projectsModel.find({
            firm: firmId,
            isDeleted: false
        }, {
            codeName: 1
        }).collation({
            locale: "en" 
        })
        .sort({
            codeName: 1
        }).limit(limit)
        .skip(offset);

        return responseModule.successResponse(req, res, {
            success: 1,
            message: '',
            data: {
                projects
            }
        });
    } catch (err) {
        logController.logEvent(req.url, req.body, req.user ? req.user._id : null, req.user ? req.user.accountType : 0, 1, err);
        return next({
            msgCode: 1
        });
    }
};

const fetchFirmProjectRoles = async (req, res, next) => {
    try{
        const projectIds = req.query.projectIds;
        const limit = parseInt(req.query.limit) || 100;
        const offset = parseInt(req.query.offset) || 0;

        const roles = await rolesModel.find({
            project: {
                $in: projectIds
            },
            isDeleted: false
        }, {
            roleType: 1
        }).collation({
            locale: "en" 
        }).sort({
            roleType: 1
        }).limit(limit)
        .skip(offset);

        return responseModule.successResponse(req, res, {
            success: 1,
            message: '',
            data: {
                roles
            }
        });
    } catch (err) {
        logController.logEvent(req.url, req.body, req.user ? req.user._id : null, req.user ? req.user.accountType : 0, 1, err);
        return next({
            msgCode: 1
        });
    }
};

const fetchProjectRolePositions = async (req, res, next) => {
    try{
        const roleIds = req.query.roleIds;

        const reviewers = await positionsModel.aggregate([
            {
              $match: {
                role: {
                    $in: roleIds
                }
              }
            },
            {
                $project: {
                    reviewer:{
                        $toObjectId:"$reviewer"
                    }
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "reviewer",
                    foreignField: "_id",
                    as: "reviewerData"
                }
            },
            {
                $unwind: {
                    path: "$reviewerData",
                    preserveNullAndEmptyArrays: true
                }
            },
            {
                $project: {
                    _id: '$reviewerData._id',
                    name: {
                        $concat: ['$reviewerData.firstName', ' ', '$reviewerData.lastName']
                    }
                }
            },
            {
                $addFields: {
                    insensitiveName: { 
                        $toLower: '$name'
                    }
                }
            },
            {
                $group: {
                    _id: '$_id',
                    name: {
                        $first: '$name'
                    },
                    insensitiveName: { 
                        $first: '$insensitiveName' 
                    }
                }
            },
            {
                $sort: {
                    insensitiveName: 1
                }
            }
        ]);

        return responseModule.successResponse(req, res, {
            success: 1,
            message: '',
            data: {
                reviewers
            }
        });
    } catch (err) {
        logController.logEvent(req.url, req.body, req.user ? req.user._id : null, req.user ? req.user.accountType : 0, 1, err);
        return next({
            msgCode: 1
        });
    }
};


const fetchFirmInvoices = async (req, res, next) => {
    try{
        const weekId = parseInt(req.query.weekId) || moment().isoWeek();
        const weekYear = parseInt(req.query.weekYear) || moment().isoWeekYear();
    
        const firmId = req.params.firmId;
        const projectIds = req.query.projectIds;
        const roleIds = req.query.roleIds;
        const reviewerIds = req.query.reviewerIds;
        const timezone = req.query.timezone || '';
        
        const filter = {
            weekId,
            firm: firmId
        };
    
        if(projectIds && projectIds.length){
            filter.project = {
                $in: projectIds
            };
        }
    
        if(roleIds && roleIds.length){
            filter.role = {
                $in: roleIds
            };
        }
    
        if(reviewerIds && reviewerIds.length){
            filter.reviewer = {
                $in: reviewerIds
            };
        }
    
        const invoices = await invoicesModel.aggregate([
            {
              $match: filter
            },
            {
                $project: {
                    reviewer:{
                        $toObjectId:"$reviewer"
                    },
                    project:{
                        $toObjectId:"$project"
                    },
                    role:{
                        $toObjectId:"$role"
                    },
                    timezone: "$timezone",
                    documentName: "$documentName",
                    totalWeekHours: "$totalWeekHours",
                    createdAt: "$createdAt",
                    weekYear: {
                        $year: {
                            date: "$weekStartDate",
                            timezone: "$timezone"
                        }
                    }
                }
            },
            {
                $match: {
                    weekYear
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "reviewer",
                    foreignField: "_id",
                    as: "reviewerData"
                }
            },
            {
                $unwind:
                  {
                    path: "$reviewerData",
                    preserveNullAndEmptyArrays: true
                  }
            },
            {
                $lookup: {
                    from: "projects",
                    localField: "project",
                    foreignField: "_id",
                    as: "projectData"
                }
            },
            {
                $unwind:
                  {
                    path: "$projectData",
                    preserveNullAndEmptyArrays: true
                  }
            },
            {
                $lookup: {
                    from: "roles",
                    localField: "role",
                    foreignField: "_id",
                    as: "roleData"
                }
            },
            {
                $unwind:
                  {
                    path: "$roleData",
                    preserveNullAndEmptyArrays: true
                  }
            },
            {
                $group: {
                    _id: "$reviewer",
                    firstName: {
                        $first: "$reviewerData.firstName"
                    },
                    lastName: {
                        $first: "$reviewerData.lastName"
                    },
                    totalWeekHours: {
                        $sum: "$totalWeekHours"
                    },
                    invoices: {
                        $push:  {
                            createdAt: "$createdAt",
                            documentName: "$documentName",
                            projectName: "$projectData.codeName",
                            role: "$roleData.roleType",
                        }
                    },
                },
            },
        ]);
    
        if(invoices && invoices.length){
            invoices.forEach(record => {
                if(record.invoices && record.invoices.length){
                    record.invoices.forEach(invoice => {
                        if(timezone && timezone !== ''){
                            invoice.createdAt = moment.tz(invoice.createdAt, timezone).format('MM/DD/YYYY hh:mm:ss A');
                        }
                    });
                }
            });
        }

        return responseModule.successResponse(req, res, {
            success: 1,
            message: '',
            data: {
                invoices
            }
        });
    } catch (err) {
        logController.logEvent(req.url, req.body, req.user ? req.user._id : null, req.user ? req.user.accountType : 0, 1, err);
        return next({
            msgCode: 1
        });
    }
};


module.exports = {
    fetchFirmsList,
    fetchFirmProjects,
    fetchFirmProjectRoles,
    fetchProjectRolePositions,
    fetchFirmInvoices
}