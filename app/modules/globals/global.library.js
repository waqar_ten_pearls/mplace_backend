const winston = require('winston');

let validationResponse = async (message, req, next) => {
    let result = await req.getValidationResult();

    if (!result.isEmpty()) {
        let errors = result.array().map(error => {
            return error.message;
        });
        winston.error(message + errors.join(' && '));
        return next({
            msgCode: errors[0]
        });
    }

    return next();
};

Object.size = obj => {
    let size = 0, 
        key;

    for (key in obj) {
        if (obj.hasOwnProperty(key)){
          ++ size
        }
    }

    return size;
};

const trimInputParams = paramsObj => {
    if(Object.size(paramsObj)){
        let key;
        for (key in paramsObj) {
            if(typeof paramsObj[key] === 'string'){
                paramsObj[key] = paramsObj[key].trim();
            }
        }
    }

    return paramsObj;
};

const isValidObjectId = value => {
    return new RegExp('^[0-9a-fA-F]{24}$').test(value);
};

module.exports = {
    validationResponse,
    trimInputParams,
    isValidObjectId
}