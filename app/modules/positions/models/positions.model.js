'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let positionsSchema = new schema({
    role: {
        type: String,
        required: true
    },
    reviewer: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: {}
});

module.exports = mongoose.model('positions', positionsSchema);