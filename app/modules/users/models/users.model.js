'use strict';

const mongoose = require('mongoose'),
    schema = mongoose.Schema;

let usersSchema = new schema({
    email: {
        type: String,
        required: true
    },       
    firstName: {
        type: String,
        default: ''
    },    
    lastName: {
        type: String,
        default: ''
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: {}
});

usersSchema.index({
    email: 1
}, {
    unique: true
});

module.exports = mongoose.model('users', usersSchema);