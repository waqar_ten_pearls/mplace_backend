'use strict';

const glob = require('glob'),
    winston = require('winston');

module.exports = app => {
    winston.info('Loading Routes...');
    glob.sync('app/modules/**/*.routes.js').forEach(file => {
        require('../' + file)(app, '/api/v1/');
        winston.info(file + ' is loaded');
    });
};