const _ = require('lodash'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    cors = require('cors'),
    express = require('express'),
    expressListeners = require('./expressListeners'),
    expressValidator = require('express-validator'),
    flash = require('connect-flash'),
    http = require('http'),
    logger = require('morgan'),
    path = require('path'),
    session = require('express-session'),
    validators = require('./validators'),
    winston = require('winston'),
    mongoStore = require('connect-mongo')(session),
    chalk = require('chalk');

const app = express();

let corsOptionsDelegate = (req, callback) => {
    let corsOptions;
    if (config.allowedDomains.indexOf(req.header('Origin')) !== -1) {
        corsOptions = {
            credentials: true,
            origin: true
        };
    } else {
        corsOptions = {
            origin: false
        };
    }
    callback(null, corsOptions);
};

app.set('views', path.join(__dirname, '../app/views'));
app.set('view engine', 'ejs');
app.locals.moment = require('moment');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use(expressValidator(validators.validatorFunctions));

require('./config')(err => {
    if (err) {
        winston.error(err);
    } else {

        function normalizePort(val) {
            var port = parseInt(val, 10);

            if (isNaN(port)) {
                // named pipe
                return val;
            }

            if (port >= 0) {
                // port number
                return port;
            }

            return false;
        }

        let server = http.createServer(app);

        if (process.env.NODE_ENV === 'production') {
            let port = normalizePort(config.PORT ? `${config.PORT}${process.env.NODE_APP_INSTANCE}` : '3000');
            server.listen(port);
        } else {
            server.listen(parseInt(config.PORT));
        }

        server.on('error', expressListeners.onError);
        server.on('request', expressListeners.onRequest);
        server.on('listening', expressListeners.onListening);

        app.use(cors(corsOptionsDelegate));
        app.use(cookieParser());

        app.use(session({
            secret: config.session.secret,
            store: new mongoStore({
                url: config.mongodb.host + config.mongodb.db_name,
                ttl: 14 * 24 * 60 * 60,
                clear_interval: 3600
            }),
            saveUninitialized: false,
            clearExpired: true,
            resave: true,
            checkExpirationInterval: 900000,
            cookie: {
                maxAge: 1000 * 3600 * 24 * 100
            }, // remember for 7 days
        }));

        app.use(flash());

        require('./routes')(app);

        app.get('/', (req, res, next) => {
            return res.json({
                success: 1,
                message: 'MPlace is running',
                response: 200,
                data: {}
            });
        });

        let errors = require('./errors');

        // catch 404 and forward to error handler
        app.use((req, res, next) => {
            let err = new Error();
            err.status = 404;
            next(err);
        });

        app.use((err, req, res, next) => {
            let response = 200;

            if (err.status === 404) {
                response = 404;
                err.msgCode = 4;
            }

            if (!err.msgCode) {
                console.log(err);
                res.status(500);
                response = 500;
                err.msgCode = 8;
            } else if (err.msgCode === 2) {
                response = 401;
                res.status(401);
            } else if (err.msgCode === 3) {
                res.status(403);
            }

            let errorMsg = errors[err.msgCode].msg.EN;
            if (errorMsg.includes('<placeholder>')) {
                errorMsg = errorMsg.replace('<placeholder>', err.placeholder || '');
            }

            if (err.msgCode) {
                winston.error(err.msgCode + ' ' + errorMsg);
            } else if (err) {
                winston.error(err);
            }

            return res.json({
                success: 0,
                message: errorMsg,
                response,
                data: {}
            });
        });
    }
});

module.exports = app;
