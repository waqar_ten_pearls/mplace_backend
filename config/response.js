'use strict'; // NOSONAR

let url = require('url'),
    errors = require('./errors');

let successResponse = async (req, res, data) => {
    req.output = data;

    data.response = 200;
    res.json(data);
};

let errorResponseWithData = async (res, data, err) => {
    res.json({
        success: 0,
        response: 200,
        message: errors[err.msgCode].msg.EN,
        data
    });
};

module.exports = {
    successResponse,
    errorResponseWithData
};
