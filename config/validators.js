const moment = require('moment');
const momentTimezone = require('moment-timezone');

let validatorFunctions = {
	errorFormatter: (param, message, value) => {
    	return {
        	param,
        	message,
        	value
    	};
	},
	customValidators: {
	    isNumber: value => {
	        return typeof value === 'number';
		},
		isValidObjectId: value => {
			return new RegExp('^[0-9a-fA-F]{24}$').test(value);
		},
		isValidWeekId: value => {
			return value >= 1 && value <= 53;	
		},
		isValidYear: value => {
			return value >= 1900 && value <= moment().year();
		},
		isValidArrayOfObjectIds: array => {
			let flag = true;

			if(array && array.length){
				const len = array.length;
				let i = 0;
		
				for(i; i< len; ++i){
					if(!new RegExp('^[0-9a-fA-F]{24}$').test(array[i])){
						flag = false;
						break;
					}
				}
			}

			return flag;
		},
		notEmptyArray: array => {
			return array.length;
		},
		isValidTimezone: value => {
			return !!moment.tz.zone(value);
		}
	}
};

3
module.exports = {
	validatorFunctions
}