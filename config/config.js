var glob = require('glob'),
    path = require('path'),
    chalk = require('chalk'),
    env = process.env.NODE_ENV || 'development',
    mongoose = require('mongoose'),
    asyncLib = require('async'),
    _ = require('lodash'),
    winston = require('winston'),
    fs = require('fs'),
    autoIncrement = require('mongoose-auto-increment');

global.config = {};

module.exports = callback => {
    asyncLib.series([
        envCb => {
            // configuring the environment
            glob('config/env/**/*.json', (err, files) => {

                if (err) {
                    return envCb(err);
                } else {
                    // picking up the environment file
                    config = require(path.join(__dirname, 'env', env + '.json'));
                    _.extend(config, JSON.parse(fs.readFileSync(path.join(__dirname, 'config.json'), 'utf-8')));

                    if (!config) {
                        return envCb('Error occured while loading config file!');
                    } else {
                        winston.info('Config File Loaded ...:', chalk.yellow(env));
                        let dbURI = config.mongodb.host + config.mongodb.db_name;
                        // make connection with mongodb
                        if (!mongoose.connection.readyState) {
                            mongoose.connect(dbURI, { 
                                useUnifiedTopology: true, 
                                useNewUrlParser: true, 
                                useCreateIndex: true,
                                useFindAndModify: false
                            });

                            autoIncrement.initialize(mongoose.connection);
                        } else {
                            return envCb();
                        }

                        // when successfully connected
                        mongoose.connection.on('connected', () => {
                            winston.info('mongoose connection open to ' + dbURI);
                            return envCb();
                        });

                        // if the connection throws an error
                        mongoose.connection.on('error', err => {
                            winston.error(err);
                            return envCb(err);
                        });

                        // when the connection is disconnected
                        mongoose.connection.on('disconnected', () => {
                            return envCb('mongoose connection disconnected');
                        });
                    }
                }
            });
        },
        modelsCb => {
            // load all models
            glob('app/modules/**/*.model.js', (err, files) => {
                if (err) {
                    return modelsCb(err);
                } else {
                    winston.info('Loading Models ...');
                    files.forEach(file => {
                        require(path.join(__dirname, '../', file));
                        winston.info(file, ' loaded');
                    });

                    return modelsCb();
                }
            });
        }
    ], err => {
        if (err) {
            return callback(err);
        } else {
            return callback();
        }
    });
};