const chalk = require('chalk'),
    url = require('url'),
    moment = require('moment'),
    winston = require('winston');

let onError = error => {
    if (error.syscall !== 'listen') {
        throw error;
    }
    switch (error.code) {
        case 'EACCES':
            winston.error('Port ' + config.PORT + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            winston.error('Port ' + config.PORT + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
};

let onRequest = (req, res) => {
    if (req.url && !req.url.includes('upload')) {
        let body = [];

        req.on('data', chunk => {
            body.push(chunk);
        }).on('end', () => {
            body = Buffer.concat(body).toString();
            winston.info(body);
        });
    }

    res.on('finish', () => {
        winston.info('API hit by: ' + (req.user ? chalk.bold.green(req.user._id + ' ' + req.user.email) : chalk.bold.yellow('Guest User')) + ' ' + moment().format('DD-MMM-YYYY hh:mmA'));
        
        if(process.env.NODE_ENV !== 'production' && req.output){
            winston.info(JSON.stringify(req.output.data));
        }
    });
};

let onListening = server => {
    if (process.env.NODE_ENV === 'production') {
        winston.info(chalk.bold.green('Server is listening on port', `${config.PORT}${process.env.NODE_APP_INSTANCE}`));
    } else {
        winston.info(chalk.bold.green('Server is listening on port', `${config.PORT}`));
    }
};

module.exports = {
    onError,
    onRequest,
    onListening
};
