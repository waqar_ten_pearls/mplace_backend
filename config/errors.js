const glob = require('glob'),
    _ = require('lodash'),
    fs = require('fs'),
    winston = require('winston');

winston.info('Loading Error Messages ...');
let routePath = 'app/modules/**/*.errors.json',
    errorObject = {
        1: {
            msg: {
                EN: 'Error occured. Please try again later.'
            }
        },
        2: {
            msg: {
                EN: 'Please sign-in to proceed.'
            }
        },
        3: {
            msg: {
                EN: 'Not authorized.'
            }
        },
        4: {
            msg: {
                EN: 'Requested URL not found.'
            }
        },
        
    };

glob.sync(routePath).forEach(function(file) {
    _.extend(errorObject, JSON.parse(fs.readFileSync(file, 'utf-8')));
    winston.info(file + ' is loaded');
});

module.exports = errorObject;
